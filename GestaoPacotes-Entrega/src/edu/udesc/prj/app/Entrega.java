package edu.udesc.prj.app;

import edu.udesc.prj.ui.frmAutenticacaoEntrega;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

/**
 *
 * @author Guilherme Pellizzetti e Vinicius Cardoso
 */
public class Entrega {
    
    

    private static void populaDados() {

        File arquivoUsuario = new File("Usuarios.txt");
        File arquivoCidade = new File("Cidades.txt");

        try {

            if (!arquivoUsuario.exists()) {
                arquivoUsuario.createNewFile();

                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(arquivoUsuario, true), "UTF-8"
                ));

                out.write("Usuario|Senha|Categoria|Cidade\n");
                out.write("1|1|Atendimento|Rio do Sul\n");
                out.write("2|2|Cliente|Rio do Sul\n");
                out.write("3|3|Triagem|Ibirama\n");
                out.write("4|4|Entrega|Floripa");

                out.close();

            }

            if (!arquivoCidade.exists()) {
                arquivoCidade.createNewFile();

                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(arquivoCidade, true), "UTF-8"
                ));

                out.write("Cidade\n");
                out.write("Abdon Batista\n");
                out.write("Abelardo Luz\n");
                out.write("Agrolandia\n");
                out.write("Agronomica\n");
                out.write("Agua Doce\n");
                out.write("Aguas Frias\n");
                out.write("Aguas Mornas\n");
                out.write("Aguas de Chapeco\n");
                out.write("Alfredo Wagner\n");
                out.write("Alto Bela Vista\n");
                out.write("Anchieta\n");
                out.write("Angelina\n");
                out.write("Anita Garibaldi\n");
                out.write("Anitapolis\n");
                out.write("Antonio Carlos\n");
                out.write("Apiuna\n");
                out.write("Arabuta\n");
                out.write("Araquari\n");
                out.write("Ararangua\n");
                out.write("Armazem\n");
                out.write("Arroio Trinta\n");
                out.write("Arvoredo\n");
                out.write("Ascurra\n");
                out.write("Atalanta\n");
                out.write("Aurora\n");
                out.write("Balneario Arroio do Silva\n");
                out.write("Balneario Barra do Sul\n");
                out.write("Balneario Camboriu\n");
                out.write("Balneario Gaivota\n");
                out.write("Bandeirante\n");
                out.write("Barra Bonita\n");
                out.write("Barra Velha\n");
                out.write("Bela Vista do Toldo\n");
                out.write("Belmonte\n");
                out.write("Benedito Novo\n");
                out.write("Biguacu\n");
                out.write("Blumenau\n");
                out.write("Bocaina do Sul\n");
                out.write("Bom Jardim da Serra\n");
                out.write("Bom Jesus do Oeste\n");
                out.write("Bom Jesus\n");
                out.write("Bom Retiro\n");
                out.write("Bombinhas\n");
                out.write("Botuvera\n");
                out.write("Braco do Norte\n");
                out.write("Braco do Trombudo\n");
                out.write("Brunopolis\n");
                out.write("Brusque\n");
                out.write("Cacador\n");
                out.write("Caibi\n");
                out.write("Calmon\n");
                out.write("Camboriu\n");
                out.write("Campo Alegre\n");
                out.write("Campo Belo do Sul\n");
                out.write("Campo Ere\n");
                out.write("Campos Novos\n");
                out.write("Canelinha\n");
                out.write("Canoinhas\n");
                out.write("Capao Alto\n");
                out.write("Capinzal\n");
                out.write("Capivari de Baixo\n");
                out.write("Catanduvas\n");
                out.write("Caxambu do Sul\n");
                out.write("Celso Ramos\n");
                out.write("Cerro Negro\n");
                out.write("Chapadao do Lageado\n");
                out.write("Chapeco\n");
                out.write("Cocal do Sul\n");
                out.write("Concordia\n");
                out.write("Cordilheira Alta\n");
                out.write("Coronel Freitas\n");
                out.write("Coronel Martins\n");
                out.write("Correia Pinto\n");
                out.write("Corupa\n");
                out.write("Criciuma\n");
                out.write("Cunha Pora\n");
                out.write("Cunhatai\n");
                out.write("Curitibanos\n");
                out.write("Descanso\n");
                out.write("Dionisio Cerqueira\n");
                out.write("Dona Emma\n");
                out.write("Doutor Pedrinho\n");
                out.write("Entre Rios\n");
                out.write("Ermo\n");
                out.write("Erval Velho\n");
                out.write("Faxinal dos Guedes\n");
                out.write("Flor do Sertao\n");
                out.write("Florianopolis\n");
                out.write("Formosa do Sul\n");
                out.write("Forquilhinha\n");
                out.write("Fraiburgo\n");
                out.write("Frei Rogerio\n");
                out.write("Galvao\n");
                out.write("Garopaba\n");
                out.write("Garuva\n");
                out.write("Gaspar\n");
                out.write("Governador Celso Ramos\n");
                out.write("Grao Para\n");
                out.write("Gravatal\n");
                out.write("Guabiruba\n");
                out.write("Guaraciaba\n");
                out.write("Guaramirim\n");
                out.write("Guaruja do Sul\n");
                out.write("Guatambu\n");
                out.write("Herval d'Oeste\n");
                out.write("Ibiam\n");
                out.write("Ibicare\n");
                out.write("Ibirama\n");
                out.write("Icara\n");
                out.write("Ilhota\n");
                out.write("Imarui\n");
                out.write("Imbituba\n");
                out.write("Imbuia\n");
                out.write("Indaial\n");
                out.write("Iomere\n");
                out.write("Ipira\n");
                out.write("Ipora do Oeste\n");
                out.write("Ipuacu\n");
                out.write("Ipumirim\n");
                out.write("Iraceminha\n");
                out.write("Irani\n");
                out.write("Irati\n");
                out.write("Irineopolis\n");
                out.write("Ita\n");
                out.write("Itaiopolis\n");
                out.write("Itajai\n");
                out.write("Itapema\n");
                out.write("Itapiranga\n");
                out.write("Itapoa\n");
                out.write("Ituporanga\n");
                out.write("Jabora\n");
                out.write("Jacinto Machado\n");
                out.write("Jaguaruna\n");
                out.write("Jaragua do Sul\n");
                out.write("Jardinopolis\n");
                out.write("Joacaba\n");
                out.write("Joinville\n");
                out.write("Jose Boiteux\n");
                out.write("Jupia\n");
                out.write("Lacerdopolis\n");
                out.write("Lages\n");
                out.write("Laguna\n");
                out.write("Lajeado Grande\n");
                out.write("Laurentino\n");
                out.write("Lauro Muller\n");
                out.write("Lebon Regis\n");
                out.write("Leoberto Leal\n");
                out.write("Lindoia do Sul\n");
                out.write("Lontras\n");
                out.write("Luiz Alves\n");
                out.write("Luzerna\n");
                out.write("Macieira\n");
                out.write("Mafra\n");
                out.write("Major Gercino\n");
                out.write("Major Vieira\n");
                out.write("Maracaja\n");
                out.write("Maravilha\n");
                out.write("Marema\n");
                out.write("Massaranduba\n");
                out.write("Matos Costa\n");
                out.write("Meleiro\n");
                out.write("Mirim Doce\n");
                out.write("Modelo\n");
                out.write("Mondai\n");
                out.write("Monte Carlo\n");
                out.write("Monte Castelo\n");
                out.write("Morro Grande\n");
                out.write("Morro da Fumaca\n");
                out.write("Navegantes\n");
                out.write("Nova Erechim\n");
                out.write("Nova Itaberaba\n");
                out.write("Nova Trento\n");
                out.write("Nova Veneza\n");
                out.write("Novo Horizonte\n");
                out.write("Orleans\n");
                out.write("Otacilio Costa\n");
                out.write("Ouro Verde\n");
                out.write("Ouro\n");
                out.write("Paial\n");
                out.write("Painel\n");
                out.write("Palhoca\n");
                out.write("Palma Sola\n");
                out.write("Palmeira\n");
                out.write("Palmitos\n");
                out.write("Papanduva\n");
                out.write("Paraiso\n");
                out.write("Passo de Torres\n");
                out.write("Passos Maia\n");
                out.write("Paulo Lopes\n");
                out.write("Pedras Grandes\n");
                out.write("Penha\n");
                out.write("Peritiba\n");
                out.write("Petrolandia\n");
                out.write("Picarras\n");
                out.write("Pinhalzinho\n");
                out.write("Pinheiro Preto\n");
                out.write("Piratuba\n");
                out.write("Planalto Alegre\n");
                out.write("Pomerode\n");
                out.write("Ponte Alta do Norte\n");
                out.write("Ponte Alta\n");
                out.write("Ponte Serrada\n");
                out.write("Porto Belo\n");
                out.write("Porto Uniao\n");
                out.write("Pouso Redondo\n");
                out.write("Praia Grande\n");
                out.write("Presidente Castelo Branco\n");
                out.write("Presidente Getulio\n");
                out.write("Presidente Nereu\n");
                out.write("Princesa\n");
                out.write("Quilombo\n");
                out.write("Rancho Queimado\n");
                out.write("Rio Fortuna\n");
                out.write("Rio Negrinho\n");
                out.write("Rio Rufino\n");
                out.write("Rio d'Oeste\n");
                out.write("Rio das Antas\n");
                out.write("Rio do Campo\n");
                out.write("Rio do Sul\n");
                out.write("Rio dos Cedros\n");
                out.write("Riqueza\n");
                out.write("Rodeio\n");
                out.write("Romelandia\n");
                out.write("Salete\n");
                out.write("Saltinho\n");
                out.write("Salto Veloso\n");
                out.write("Sangao\n");
                out.write("Santa Cecilia\n");
                out.write("Santa Helena\n");
                out.write("Santa Rosa de Lima\n");
                out.write("Santa Rosa do Sul\n");
                out.write("Santa Terezinha do Progresso\n");
                out.write("Santa Terezinha\n");
                out.write("Santiago do Sul\n");
                out.write("Santo Amaro da Imperatriz\n");
                out.write("Sao Bento do Sul\n");
                out.write("Sao Bernardino\n");
                out.write("Sao Bonifacio\n");
                out.write("Sao Carlos\n");
                out.write("Sao Cristovao do Sul\n");
                out.write("Sao Domingos\n");
                out.write("Sao Francisco do Sul\n");
                out.write("Sao Joao Batista\n");
                out.write("Sao Joao do Itaperiu\n");
                out.write("Sao Joao do Oeste\n");
                out.write("Sao Joao do Sul\n");
                out.write("Sao Joaquim\n");
                out.write("Sao Jose do Cedro\n");
                out.write("Sao Jose do Cerrito\n");
                out.write("Sao Jose\n");
                out.write("Sao Lourenco d'Oeste\n");
                out.write("Sao Ludgero\n");
                out.write("Sao Martinho\n");
                out.write("Sao Miguel d'Oeste\n");
                out.write("Sao Miguel da Boa Vista\n");
                out.write("Sao Pedro de Alcantara\n");
                out.write("Saudades\n");
                out.write("Schroeder\n");
                out.write("Seara\n");
                out.write("Serra Alta\n");
                out.write("Sideropolis\n");
                out.write("Sombrio\n");
                out.write("Sul Brasil\n");
                out.write("Taio\n");
                out.write("Tangara\n");
                out.write("Tigrinhos\n");
                out.write("Tijucas\n");
                out.write("Timbe do Sul\n");
                out.write("Timbo Grande\n");
                out.write("Timbo\n");
                out.write("Tres Barras\n");
                out.write("Treviso\n");
                out.write("Treze Tilias\n");
                out.write("Treze de Maio\n");
                out.write("Trombudo Central\n");
                out.write("Tubarao\n");
                out.write("Tunapolis\n");
                out.write("Turvo\n");
                out.write("Uniao do Oeste\n");
                out.write("Urubici\n");
                out.write("Urupema\n");
                out.write("Urussanga\n");
                out.write("Vargeao\n");
                out.write("Vargem Bonita\n");
                out.write("Vargem\n");
                out.write("Vidal Ramos\n");
                out.write("Videira\n");
                out.write("Vitor Meireles\n");
                out.write("Witmarsum\n");
                out.write("Xanxere\n");
                out.write("Xavantina\n");
                out.write("Xaxim\n");
                out.write("Zortea");

                out.close();

            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {

                populaDados();

                frmAutenticacaoEntrega dlgAutenticacao = new frmAutenticacaoEntrega(null, true);
                dlgAutenticacao.setLocationRelativeTo(null);
                dlgAutenticacao.setVisible(true);
            }
        });
    }

}
