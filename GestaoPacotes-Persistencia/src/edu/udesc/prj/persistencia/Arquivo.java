package edu.udesc.prj.persistencia;

import java.io.*;

/**
 *
 * @author Guilherme Pellizzetti e Vinicius Cardoso
 */
public class Arquivo {

    public static void gravaArquivo(File arquivo, String linha) throws Exception {

        try {
            
            if (!arquivo.exists()) {
                arquivo.createNewFile();
            }
            
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(arquivo, true), "UTF-8"
            ));

            out.write(linha);
            out.close();

        } catch (FileNotFoundException e) {
            throw new Exception("Arquivo não encontrado!", e);
        } catch (IOException e) {
            throw new Exception("IOException - Erro ao ler arquivo", e);
        }

    }

}
