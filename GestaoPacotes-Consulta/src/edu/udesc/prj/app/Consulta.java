package edu.udesc.prj.app;

import edu.udesc.prj.ui.frmConsulta;
import java.awt.Dimension;
import java.awt.Toolkit;

/**
 *
 * @author Guilherme Pellizzetti e Vinicius Cardoso
 */
public class Consulta {

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {

                frmConsulta janConsulta = new frmConsulta();
                
                Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                janConsulta.setBounds(0,0,screenSize.width * 80 / 100, screenSize.height * 80 / 100);
                janConsulta.setLocationRelativeTo(null);
                janConsulta.setVisible(true);
            }
        });
    }

}
